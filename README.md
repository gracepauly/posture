# Getting Started with a Web Application Project - Posture

## Required Prerequisite Installation are Node.js and MongoDb

## How to install and build the project

### Open visual Studio code or command prompt from a new folder and run below commands

In the project directory, you can run:

### `git clone https://bitbucket.org/gracepauly/posture/src/master .` 
### `npm install`
### `node index.js`

Runs the app in the development mode.\
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

You will also see any  errors in the console.

# MIT License

Copyright (c) 2022 Grace-pauly

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so.
