const express = require('express');
const path = require('path');
const { check, validationResult } = require('express-validator');
const { RSA_PSS_SALTLEN_DIGEST } = require('constants');
const session = require('express-session');
const fileUpload = require('express-fileupload');

// Setup DB connection
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/posturepage', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});  // Path of Database

const Posture = mongoose.model('Posture', {
    name: String,
    title: String,
    content: String,
    imageName: String
});

const Admin = mongoose.model('Admin', {
    username: String,
    password: String
});

var myApp = express();
myApp.use(express.urlencoded({ extended: true }));
myApp.use(fileUpload());
myApp.use(session({
    secret: "posturesecret",
    resave: false,
    saveUninitialized: true
}));

myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');

//home page
myApp.get('/', function (req, res) {
    Posture.find({}).exec(function (err, postures) {
        console.log(err);
        req.session.pages = postures;
        res.render('home', { pages: req.session.pages });
    });
});

//login page
myApp.get('/login', function (req, res) {
    res.render('login', { pages: req.session.pages });
});

myApp.post('/login', function (req, res) {
    var user = req.body.username;
    var pass = req.body.password;

    Admin.findOne({ username: user, password: pass }).exec(function (err, admin) {
        //log any errors
        console.log("Error: " + err);
        console.log("Admin: " + admin);
        if (admin) {
            //Store username in session and set loggin in true
            req.session.username = admin.username;
            req.session.userLoggedIn = true;
            //Redirect user to the dashboard - form page.
            res.redirect('/');
        }
        else {
            //Display error if user info is incorrect.
            res.render('login', { error: "Sorry Login Failed!" });
        }
    });
});

myApp.get('/form', function (req, res) {
    if (req.session.userLoggedIn) {
        res.render('form', { pages: req.session.pages });
    }
    else {
        redirectToLogin(req, res);
    }
});

myApp.post('/form', [
    check('name', 'Name is required!').notEmpty(),
    check('title', 'Title is required!').notEmpty(),
    check('content', 'Content is required!').notEmpty()

], function (req, res) {
    const errors = validationResult(req);
    console.log(errors); // This will display the array of errors. 
    if (!errors.isEmpty()) {
        res.render('form', {
            errors: errors.array(),
            pages: req.session.pages
        })
    }
    else {
        var name = req.body.name;
        var title = req.body.title;
        var content = req.body.content;
        if(!req.files)
        {
            res.render('form', { 
                errors : [{msg:'Please select a image'}]  
            });
            return;
        }
        var imageName = req.files.myimage.name;
        var image = req.files.myimage;
        var imagePath = 'public/images/upload/' + imageName;
        image.mv(imagePath, function (err) {
            console.log(err);
        });

        var postureData = {
            name: name,
            title: title,
            content: content,
            imageName: imageName
        }
        //Crate an object for the model - Order
        var myPosture = new Posture(postureData);

        // Save the order
        myPosture.save().then(function (posture) {
            console.log("New page Created!");
            req.session.pages.push(posture);
            res.redirect('/page/' + posture._id);
        });
    }
});

const redirectToLogin = function (req, res) {
    res.redirect('/login');
}

const getAllPostures = function (req, res) {
    if (req.session.userLoggedIn) {
        Posture.find({}).exec(function (err, postures) {
            console.log(err);
            req.session.pages = postures;
            res.render('postures', { postures: postures, pages: req.session.pages });
        });
    }
    else {
        redirectToLogin(req, res);
    }
}

myApp.get('/postures', function (req, res) {
    getAllPostures(req, res);
});

//Logout Page
myApp.get('/logout', function (req, res) {
    req.session.username = '';
    req.session.userLoggedIn = false;
    res.redirect('/login');
});

// Delete Page
myApp.get('/delete/:id', function (req, res) {
    //Check if user session is created
    if (req.session.username) {
        //Delete
        var id = req.params.id;
        Posture.findByIdAndDelete({ _id: id }).exec(function (err, posture) {
            console.log("Error: " + err);
            let message = '';
            if (posture) {
                req.session.pages = req.session.pages.filter(item => item._id !== id)
                message = "Successfully Deleted...!";
            }
            else {
                message = "Sorry, record not deleted...!"
            }
            res.render('delete', { message: message, pages: req.session.pages });
        });
    }
    else {
        redirectToLogin(req, res);
    }
});

myApp.get('/page/:id', function (req, res) {
    var id = req.params.id;
    console.log(id);
    Posture.findOne({ _id: id }).exec(function (err, posture) {
        console.log("Error: " + err);
        if (posture) {
            res.render('page', { posture: posture, pages: req.session.pages });
        }
        else {
            res.send('No posture found with this id...!');
        }
    });

});

// Edit Page 
myApp.get('/edit/:id', function (req, res) {
    //Check if user session is created
    if (req.session.username) {
        //Edit
        var id = req.params.id;
        console.log(id);
        Posture.findOne({ _id: id }).exec(function (err, posture) {
            console.log("Error: " + err);
            if (posture) {
                res.render('edit', { posture: posture, pages: req.session.pages });
            }
            else {
                res.send('No posture found with this id...!');
            }
        });
    }
    else {
        redirectToLogin(req, res);
    }
});

//Edit Post Method
myApp.post('/edit/:id', [

    check('name', 'Name is required!').notEmpty(),
    check('title', 'Title is required!').notEmpty(),
    check('content', 'Content is required').notEmpty()

], function (req, res) {
    const errors = validationResult(req);
    console.log(errors); // This will display the array of errors. 
    if (!errors.isEmpty()) {
        var id = req.params.id;
        console.log(id);
        Posture.findOne({ _id: id }).exec(function (err, posture) {
            console.log("Error: " + err);
            console.log("Posture: " + posture);
            if (posture) {

                res.render('edit',
                    {
                        posture: posture, errors: errors.array(),
                        pages: req.session.pages
                    });
            }
            else {
                res.send('No posture found with this id...!');
            }
        });
    }

    else {
        // No Errors:
        var name = req.body.name;
        var title = req.body.title;
        var content = req.body.content;
        var imageName;
        var id = req.params.id;
        if (req.files && req.files.myimage) {
            imageName = req.files.myimage.name;
            var image = req.files.myimage;
            var imagePath = 'public/images/upload/' + imageName;
            image.mv(imagePath, function (err) {
                console.log(err);
            });

        }
        else {
            const pages = req.session.pages.filter(item=> item._id === id);
            imageName = pages[0].imageName;

        }

        var postureData = {
            name: name,
            title: title,
            content: content,
            imageName: imageName,
            
        }
     
        Posture.findOne({ _id: id }).exec(function (err, posture) {
            posture.name = name;
            posture.title = title;
            posture.content = content;

            posture.imageName = imageName;

            posture.save();
            req.session.pages.forEach(item => {
                if (item._id === id) {
                    item.name = name;
                    item.title = title;
                    item.content = content;
                    item.imageName = imageName;
            
                }
            })
            res.render('editsuccess', { postureData, pages: req.session.pages });
        });

    }
});

//start server and listen to port
myApp.listen(8080); // Open URL in Browser: http://localhost:8080
//Confirmation Output
console.log('Execution Complete... Website opened at port 8080!');

